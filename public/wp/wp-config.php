<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '($=q*-]yV]6Q?L$Y8 ^_Jj#z>S.ivXL2]D]{]3H<%ga(f:_LnK)MeJDff<YYqJGA');
define('SECURE_AUTH_KEY',  '9,ok/buh=~!1jz0/H~9[0imd4,;9U($7A)-RJy|Q9,{-gIAA.eBsuaIb66m=h<Vw');
define('LOGGED_IN_KEY',    'D8V=2,V?~ZLvm!|(-Wz_His[-JRx9bVA.NO%n-%%jwM`+bQ,l:([hVUR9iWn >;`');
define('NONCE_KEY',        '1>M=9#R#+t`P]Qg*X76i524y9i!PPogJ?a/TS>>Gaz{%aF)U;Ndjc!8%W7MW^6=W');
define('AUTH_SALT',        'wm&z<Y:@#t8b}Ph>hoZbRj{~B|zLrgxMa}>t?!@!<Vzb}&nO,&(E$TBvSjX4PF)C');
define('SECURE_AUTH_SALT', 'q@u-lM|o)K6[a5lmT}3)> A#G.3/++vnp<SP}4<5 w,{e$`0EU`zWq~w }h.?oi{');
define('LOGGED_IN_SALT',   '-Zv)9o;b*{KZ_x<5;!Owi<7R`^P[;N*7YGG++>#WS_j1$dm/iTc6d+KHo`de%YJ/');
define('NONCE_SALT',       'V]+w@LN(YuWAHy(!^Ov9NA(7D@*5,zg%EUyCNy0@<[J&x~[V+h]rZ&DN0Fmc0X$T');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
