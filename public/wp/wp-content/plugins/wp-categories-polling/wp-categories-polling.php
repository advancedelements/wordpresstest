<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.leanj.de/
 * @since             1.0.0
 * @package           Wp_Categories_Polling
 *
 * @wordpress-plugin
 * Plugin Name:       Wp Categories Polling
 * Plugin URI:        http://www.leanj.de/
 * Description:       This plugin polls a fake API every 30 minutes to check for changes to categories.
 * Version:           1.0.0
 * Author:            Jason Dsouza
 * Author URI:        http://www.leanj.de/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-categories-polling
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

define( 'PLUGIN_NAME_VERSION', '1.0.0' );

require_once plugin_dir_path( dirname( __FILE__ ) ) .'wp-categories-polling/includes/class-wp-categories-polling-initiate.php';

$plugin_initiate_polling = new Wp_Categories_Polling_initate();

add_filter('cron_schedules', array($plugin_initiate_polling, 'wp_categories_polling_schedules'));

add_action('wp_categories_polling_schedule_hook', array($plugin_initiate_polling, 'wp_categories_polling_schedule'));
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-wp-categories-polling-activator.php
 */
function activate_wp_categories_polling() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-categories-polling-activator.php';
	Wp_Categories_Polling_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-wp-categories-polling-deactivator.php
 */
function deactivate_wp_categories_polling() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-categories-polling-deactivator.php';
	Wp_Categories_Polling_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_wp_categories_polling' );
register_deactivation_hook( __FILE__, 'deactivate_wp_categories_polling' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-categories-polling.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */

function run_wp_categories_polling() {

	$plugin = new Wp_Categories_Polling();
	$plugin->run();

}

if(is_admin()) {
	run_wp_categories_polling();
}