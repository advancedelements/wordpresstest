<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://www.leanj.de/
 * @since      1.0.0
 *
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
