<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.leanj.de/
 * @since      1.0.0
 *
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 * @author     Jason Dsouza <jason.rgd@gmail.com>
 */
class Wp_Categories_Polling_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
		wp_clear_scheduled_hook('wp_categories_polling_schedule_hook');
	}

}
