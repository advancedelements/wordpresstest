<?php

/**
 * Define the General Settings functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.leanj.de/
 * @since      1.0.0
 *
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 */

/**
 * Define the eneral Settings functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 * @author     Jason Dsouza <jason.rgd@gmail.com>
 */

class Wp_Categories_Polling_setting {

    function register_fields() {
        register_setting( 'general', 'fetch_categories', 'esc_attr' );
        add_settings_field('fetch_categories', '<a href="'.add_query_arg('fetch_categories', 1).'" >'.__('Fetch Categories' , 'fetch_categories' ).'</a>' , array(&$this, 'fields_html') , 'general' );


    }

    function wp_categories_polling_add_query_vars_filter( $vars ){
	    $vars[] = "fetch_categories";
	    return $vars;
	}

    function fields_html() {
    	$fetch_categories = $_GET['fetch_categories'];
    	if ($fetch_categories == 1) {
			$plugin_initiate_polling = new Wp_Categories_Polling_initate();
    		$plugin_initiate_polling->wp_categories_polling_schedule();
    	}
    }
}