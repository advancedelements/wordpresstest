<?php

/**
 * Define the General Settings functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.leanj.de/
 * @since      1.0.0
 *
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 */

/**
 * Define the eneral Settings functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 * @author     Jason Dsouza <jason.rgd@gmail.com>
 */



class Wp_Categories_Polling_initate {

    public function wp_categories_polling_schedules($schedules){
    	if(!isset($schedules["5min"])){
        $schedules["5min"] = array(
            'interval' => 5*60,
            'display' => __('Once every 5 minutes'));
    	}
    
	    if(!isset($schedules["30min"])){
	        $schedules["30min"] = array(
	            'interval' => 30*60,
	            'display' => __('Once every 30 minutes'));
	    }

    	return $schedules;
	}

	public function wp_categories_polling_schedule(){
	    // Finally, create the actual function that you would like to run:
		//all db queries go here
		$curl = curl_init();
		require_once(ABSPATH . 'wp-admin/includes/taxonomy.php'); 

		$curl = curl_init('http://localhost:3000/categories');
		curl_setopt($curl,  CURLOPT_RETURNTRANSFER, true);
		$resp = curl_exec($curl);
		curl_close($curl);

		$jsonArr = json_decode($resp,true);

		//var_dump($jsonArr);
		$idValues = array();
		global $wpdb;

        $wpdb->query(
            $wpdb->prepare(
                "
                DELETE FROM wp_terms
		 WHERE term_id != %d
		",
                1
            )
        );

        $wpdb->query(
            $wpdb->prepare(
                "
                DELETE FROM wp_term_taxonomy
		 WHERE term_taxonomy_id != %d
		",
                1
            )
        );
		foreach ($jsonArr as $category) {
			if( is_null (get_category_to_edit($category['id'])) ) {
				$wpdb->insert('wp_terms',array( 'term_id' => $category['id'], 'name'=>$category['name']));
				if (is_null($category['parent_id'])) {
					$category['parent_id'] = 0;
				}
				$wpdb->insert('wp_term_taxonomy',array( 'term_taxonomy_id' => $category['id'], 'term_id' => $category['id'], 'taxonomy'=>'category', 'parent' => $category['parent_id']));

			}
		} 

	}
}
