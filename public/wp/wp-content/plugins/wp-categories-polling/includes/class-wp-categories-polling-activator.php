<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.leanj.de/
 * @since      1.0.0
 *
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 * @author     Jason Dsouza <jason.rgd@gmail.com>
 */
class Wp_Categories_Polling_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	    if (!wp_next_scheduled('wp_categories_polling_schedule_hook')) {
			wp_schedule_event(time(), '30min', 'wp_categories_polling_schedule_hook');
		}
	}
}