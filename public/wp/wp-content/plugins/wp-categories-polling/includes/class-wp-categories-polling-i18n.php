<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       http://www.leanj.de/
 * @since      1.0.0
 *
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Wp_Categories_Polling
 * @subpackage Wp_Categories_Polling/includes
 * @author     Jason Dsouza <jason.rgd@gmail.com>
 */
class Wp_Categories_Polling_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'wp-categories-polling',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
