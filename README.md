# WP Categories Test

## How to use (Setup Notes)
- Install Vagrant
- Clone this repository into your project
- Run ``vagrant up`` (will take 1 - 2 hrs depending on your internet speed)
- Add the following lines to your hosts file:
````
192.168.100.100 app.dev
192.168.100.100 phpmyadmin.dev
````
- Navigate to ``http://app.dev/wp/`` 
- Navigate to ``http://phpmyadmin.dev/`` (both username and password are 'root')

## Work left in progress
1. Test WordPress environment to be hosted and publicly accessible (with username/password)
work around I have submitted a vagrant configuration so a test environment can be setup.
Wordpress needs to be seperately installed. Didn't have time to automate the install
2. Disable ability to add new categories from within WordPress. 
This function is redundant as user added categories will be automatically deleted on successful cron run.

## My thoughts
A better way to approach this is to use rest hooks. The rest server on update automatically notifies consumers of the api. I am assuming that the rest server is private and client is public and possibly under one administration.

## Time Required to complete?
1) Project Setup - 2 hrs (vagrant et al)
2) Planning - 45 mins
3) Development - 3 - 4 hrs
4) Research - 1
5) Post Project Analysis and  - 45 mins